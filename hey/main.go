package main

import (
	"syscall/js"
)

func Html(_ js.Value, args []js.Value) interface{} {
	return `
		<h1>🌕 Hey World 🚀</h1>
		<h2>GoLang is amazing</h2>
		<h3>Wasm is fantastic</h3>
	`
}

func main() {

	go func() {
		js.Global().Call("startCb")
	}()

	js.Global().Set("Html", js.FuncOf(Html))

	<-make(chan bool)
}
