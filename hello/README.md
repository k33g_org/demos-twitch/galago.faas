

## Build the function

```bash
tinygo build -o hello.wasm -target wasm ./main.go
```

## Use the GalaGo CLI to deploy the wasm function

```bash
galago -cmd=deploy -name=hello -wasm=hello.wasm -ver=0.0.0
galago -cmd=data
galago -cmd=activate -name=hello -ver=0.0.0
galago -cmd=call -name=hello -json='{"name":"Bob"}'
# call another version (non activated)
galago -cmd=call -name=hello -ver=0.0.0

# scale
galago -cmd=scale -name=hello -ver=0.0.1
```


